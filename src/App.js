import React, { useState, useEffect } from 'react';
import './index.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft, faCircle, faCheckCircle, faPlus,faTrash } from '@fortawesome/free-solid-svg-icons';

const App = () => {
	// HINT: each "item" in our list names a name,
	// a boolean to tell if its been completed, and a quantity
	const [items, setItems] = useState([ // this is our state where we store items in form of an array []
		
	]);


  const [inputValue, setInputValue] = useState('');   // this saves our inputvalue and updateds it
  
	const [totalItemCount, setTotalItemCount] = useState(0); // this updates our itemcount

	const handleAddButtonClick = () => { // this handles what will happen when the btn gets clicked
		const newItem = {                 // a new const with values is declared
			itemName: inputValue,
			quantity: 1,
			isSelected: false,
		
		};

		const newItems = [...items, newItem]; // adds the newItem to the array of items

		setItems(newItems); // updates the items
		calculateTotal();
		setInputValue('');
		
	};

	const handleQuantityIncrease = (index) => {
		const newItems = [...items];

		newItems[index].quantity++;

		setItems(newItems);
		calculateTotal();
	};

	const handleQuantityDecrease = (index) => {
		const newItems = [...items];

		newItems[index].quantity--;

		setItems(newItems);
		calculateTotal();
	};

	const toggleComplete = (index) => {
		const newItems = [...items];

		newItems[index].isSelected = !newItems[index].isSelected;

		setItems(newItems);
	};

	const calculateTotal = () => {
		const totalItemCount = items.reduce((total, item) => {
			return total + item.quantity;
		}, 1);

		setTotalItemCount(totalItemCount);
	};
	const handleDelete = () =>{
		setItems([]);
		setTotalItemCount(0);
		
	}

	return (
		<div className='app-background'>
			<div className='main-container'>
				<div className='add-item-box'>
					<input value={inputValue} onChange={(event) => setInputValue(event.target.value)} className='add-item-input' placeholder='Add an item...' />
					<FontAwesomeIcon icon={faPlus} onClick={() => handleAddButtonClick()} />
				</div>
				<div className='item-list'>
					{items.map((item, index) => (
						<div className='item-container'>
							<div className='item-name' onClick={() => toggleComplete(index)}>
								{item.isSelected ? (
									<>
										<FontAwesomeIcon icon={faCheckCircle} />
										<span className='completed'>{item.itemName}</span>
									</>
								) : (
									<>
										<FontAwesomeIcon icon={faCircle} />
										<span>{item.itemName}</span>
									</>
								)}
							</div>
							<div className='quantity'>
								<button>
									<FontAwesomeIcon icon={faChevronLeft} onClick={() => handleQuantityDecrease(index)} />
								</button>
								<span> {item.quantity} </span>
								<button>
									<FontAwesomeIcon icon={faChevronRight} onClick={() => handleQuantityIncrease(index)} />
								</button>
							</div>
						</div>
					))}
				</div>
				
				<div className='total'>Total: {totalItemCount}</div>
	
			</div>
			<div className='delete'><button>
				<FontAwesomeIcon icon={faTrash} onClick={() => handleDelete()}/>
				</button></div>
		</div>
		
	);
};

export default App;